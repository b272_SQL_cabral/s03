INSERT INTO artists (name) VALUES ("Rivermaya"); 
INSERT INTO artists (name) VALUES ("Psy"); 

INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Psy 6", "2012-1-1", 2);
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "KPOP", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ulan", 234, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("214", 249, "OPM", 2);

DELETE FROM `songs` WHERE `songs`.`id` = 2;
DELETE FROM `songs` WHERE `songs`.`id` = 5;

SELECT song_name, genre FROM `songs`

CREATE DATABASE blog_db;
USE blog_db;

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(50) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    title VARCHAR(200) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE post_likes (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    post_id INT NOT NULL,
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_likes_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_post_likes_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE post_comments (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    post_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_comments_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_post_comments_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


